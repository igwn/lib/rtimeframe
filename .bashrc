# Enable color support for ls and grep
if [ -x /usr/bin/dircolors ]; then
    eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# Shortcuts for history search
export HISTSIZE=
export HISTFILESIZE=

export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
export BASH_SILENCE_DEPRECATION_WARNING=1
if [[ $- == *i* ]]
then
        bind '"\e[A": history-search-backward'
        bind '"\e[B": history-search-forward'
        bind '"\e[5C": forward-word'
        bind '"\e[5D": backward-word'
        bind '"\e[1;5C": forward-word'
        bind '"\e[1;5D": backward-word'
fi

shopt -s checkwinsize
shopt -s direxpand 2> /dev/null

# Check if running inside Docker by looking for /.dockerenv
docker=""
if [ -f /.dockerenv ]; then
    docker="\[\e[36m\]docker:"  # Cyan color for 'on docker'
fi

conda=""
if [[ $CONDA_DEFAULT_ENV != "" ]]; then
    conda="\e[90m($CONDA_DEFAULT_ENV)\[\e[0m\] "
fi

# Custom PS1 prompt with conda environment in dark bold grey and Docker in cyan
# Display [time] user@hostname:path on docker (if Docker is detected) and (conda_env) if active
export PS1="\[\e[95;1m\][\[\e[95;1m\]\t] \[\e[32;1m\]\u@$docker\h\[\e[0;97m\]:\[\e[0;94m\]\w\[\e[39m\] $\[\e[0;97m\] \n\[\e[0;97m\]\[\]$conda\[\]\[\e[0m\]$ "
[ ! -f /.dockerenv ] && return 0

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/conda/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/opt/conda/etc/profile.d/conda.sh" ]; then
        . "/opt/conda/etc/profile.d/conda.sh"
    else
        export PATH="/opt/conda/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
