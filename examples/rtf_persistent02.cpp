/**
 * @file rtf_persistent02.cpp
 * @brief A program demonstrating the usage of a custom histogram class with Doxygen comments.
 */

#include <ROOT/RTimeFrame.hxx>
#include <TRandom.h>

/**
 * @brief Main function to generate a histogram and test it.
 *  
 * @return 0 on successful execution.
 */
int main() {

    ROOT::RTimeFrame rdf(50);
    
    auto r = rdf
         .DefineSlotEntry("foo", [](unsigned int slot, ULong64_t entry) { return static_cast<int>(entry); })
         .Define("D", [](){return gRandom->Exp(1);})
         .DefinePersistent("time", [](   double& time,   double D){time += D;}, {"D"})
         .Resample<double, double, int>("time",    1, 5, 15,    {"time", "foo"})
         .Display({"time", "foo"}, 10);
    
    r->Print();

    return 0;
}