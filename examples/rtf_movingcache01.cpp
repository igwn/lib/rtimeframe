/**
 * @file rtf_movingcache01.cpp
 * @brief A program demonstrating the usage of a custom histogram class with Doxygen comments.
 */

#include <ROOT/RTimeFrame.hxx>
#include <TRandom.h>

/**
 * @brief Main function to generate a histogram and test it.
 *  
 * @return 0 on successful execution.
 */
int main() {

    ROOT::RTimeFrame rdf(50);
    
    auto r = rdf
         .DefineSlotEntry("foo", [](unsigned int slot, ULong64_t entry){return static_cast<int>(entry);})
         .Define("bar", [](int foo){return foo * foo;}, {"foo"})
         .MovingCache<int, int>({"foo", "bar"})
         .Define("D", [](int bar1, int bar2){return bar2 - bar1;}, {"bar", "bar"},   {-1, 0})
         .Display({"foo", "bar", "D"});

    r->Print();

    return 0;
}

