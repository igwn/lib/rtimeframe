// Author: Enrico Guiraud, Danilo Piparo CERN  03/2017

/*************************************************************************
 * Copyright (C) 1995-2018, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef ROOT_RTF_RLOOPMANAGER
#define ROOT_RTF_RLOOPMANAGER

#include "ROOT/RDF/RLoopManager.hxx"
namespace ROOT {

namespace Internal {
namespace RTF {

   class RProxyDS;
} // ns RDF
} // ns Internal

namespace Detail {
namespace RTF {

namespace RDFInternal = ROOT::Internal::RDF;
namespace RTFInternal = ROOT::Internal::RTF;

/// The head node of a RDF computation graph.
/// This class is responsible of running the event loop.
class RLoopManager : public ::ROOT::Detail::RDF::RLoopManager {

   friend class RTFInternal::RProxyDS;

   protected:
   void Initialise();
   void Finalise();

public:
   using ::ROOT::Detail::RDF::RLoopManager::RLoopManager;
   
   void Book(RDFInternal::RActionBase *actionPtr, bool permanent = false);
};

} // ns RDF
} // ns Detail
} // ns ROOT

#endif
