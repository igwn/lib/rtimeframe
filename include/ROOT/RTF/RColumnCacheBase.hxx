// Author: Enrico Guiraud CERN 09/2020

/*************************************************************************
 * Copyright (C) 1995-2020, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef ROOT_RTF_RCOLUMNCACHEBASE
#define ROOT_RTF_RCOLUMNCACHEBASE

#include <Rtypes.h> // Long64_t

namespace ROOT {
namespace Internal {
namespace RTF {

class RColumnCacheBase {

public:
   virtual ~RColumnCacheBase(){};

   virtual void InitialiseSlot(unsigned int slot, Long64_t startEntry) = 0;
   virtual void FinaliseSlot(unsigned int slot) = 0;

   virtual void *Get(int slot, Long64_t entry) = 0;

   virtual void Load(int slot, Long64_t entry) = 0;

   virtual void PurgeTill(int slot, Long64_t entry) = 0;

   virtual std::pair<Long64_t, Long64_t> GetStoredRange(int slot) const = 0;
};

} // namespace RTF
} // namespace Internal
} // namespace ROOT

#endif
