// Author: Enrico Guiraud CERN 09/2020

/*************************************************************************
 * Copyright (C) 1995-2020, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef ROOT_RTF_RCOLUMNCACHEREADER
#define ROOT_RTF_RCOLUMNCACHEREADER

#include <Rtypes.h> // Long64_t

#include <limits>
#include <type_traits>

#include <ROOT/RDF/RColumnReaderBase.hxx>

#include "RColumnCacheBase.hxx"

namespace ROOT {
namespace Internal {
namespace RTF {

namespace RDFDetail = ROOT::Detail::RDF;
namespace RTFDetail = ROOT::Detail::RTF;

class RColumnCacheReader final : public ROOT::Detail::RDF::RColumnReaderBase {
   int fSlot;

   RColumnCacheBase *fCache;

   void *GetImpl(Long64_t entry) final { return fCache->Get(fSlot, entry); }

public:
   RColumnCacheReader(unsigned int slot, RColumnCacheBase *cache) : fSlot(slot), fCache(cache) {}
};

} // namespace RTF
} // namespace Internal
} // namespace ROOT

#endif
