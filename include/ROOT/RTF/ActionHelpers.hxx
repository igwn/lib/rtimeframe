/*************************************************************************
 * Copyright (C) 1995-2020, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef ROOT_RTFOPERATIONS
#define ROOT_RTFOPERATIONS

#include <ROOT/RDF/ActionHelpers.hxx>

namespace ROOT {
namespace Internal {
namespace RTF {

using namespace ROOT::TypeTraits;
using namespace ROOT::VecOps;
using namespace ROOT::RDF;
using namespace ROOT::Detail::RDF;

using Hist_t = ::TH1D;

class EvalHelper : public RActionImpl<EvalHelper> {
private:
   RDefineBase *fEvalDefine;

public:
   using ColumnTypes_t = TypeList<ULong64_t>;
   EvalHelper(RDefineBase *evalDefine) : fEvalDefine(evalDefine){};
   EvalHelper(EvalHelper &&) = default;
   EvalHelper(const EvalHelper &) = delete;
   void InitTask(TTreeReader *, unsigned int) {}
   void Exec(unsigned int slot, ULong64_t entry) { fEvalDefine->Update(slot, entry); }
   void Initialize()
   { /* noop */
   }
   void Finalize() {}

   std::string GetActionName() { return "EvalHelper"; }
};

} // end of NS RDF
} // end of NS Internal
} // end of NS ROOT

/// \endcond

#endif
