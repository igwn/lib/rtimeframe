// Author: Enrico Guiraud, Danilo Piparo CERN  12/2016

/*************************************************************************
 * Copyright (C) 1995-2018, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

/**
  \defgroup timeframe TimeFrame
ROOT's RTimeFrame allows to analyse data stored in TTrees with a high level interface.
*/

#ifndef ROOT_RTIMEFRAME
#define ROOT_RTIMEFRAME

#include "TROOT.h" // To allow ROOT::EnableImplicitMT without including ROOT.h
#include "ROOT/RTF/RInterface.hxx"
#include "ROOT/RDF/Utils.hxx"
#include "ROOT/RStringView.hxx"
#include "RtypesCore.h"

#include <memory>
#include <ostream>
#include <string>
#include <vector>

class TDirectory;
class TTree;

namespace ROOT {
namespace RDF {
class RDataSource;
}

namespace RTFDetail = ROOT::Detail::RTF;

class RTimeFrame : public ROOT::RTF::RInterface<RTFDetail::RLoopManager> {
public:
   using ColumnNames_t = ROOT::RDF::ColumnNames_t;
   RTimeFrame(std::string_view treeName, std::string_view filenameglob, const ColumnNames_t &defaultBranches = {});
   RTimeFrame(std::string_view treename, const std::vector<std::string> &filenames,
              const ColumnNames_t &defaultBranches = {});
   RTimeFrame(std::string_view treeName, ::TDirectory *dirPtr, const ColumnNames_t &defaultBranches = {});
   RTimeFrame(TTree &tree, const ColumnNames_t &defaultBranches = {});
   RTimeFrame(ULong64_t numEntries);
   RTimeFrame(std::unique_ptr<ROOT::RDF::RDataSource>, const ColumnNames_t &defaultBranches = {});
};

} // ns ROOT

/// Print a RTimeFrame at the prompt
namespace cling {
std::string printValue(ROOT::RTimeFrame *tdf);
} // ns cling

#endif // ROOT_RTIMEFRAME
