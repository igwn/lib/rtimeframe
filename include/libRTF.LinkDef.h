/**
 **********************************************
 *
 * \file libRTF.LinkDef.h
 * \brief Link definitions of the library
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#ifdef __CINT__
#include "libRTF.Config.h"

#pragma link off all globals;
#pragma link off all functions;

#pragma link C++ namespace ROOT::RTF+;

#pragma link C++ class ROOT::RTimeFrame+;

#pragma link C++ class ROOT::RTF::RTrivialDS+;
#if FRAMEL_FOUND
#pragma link C++ class ROOT::RTF::RGwfDS+;
#endif
#if HDF5_FOUND
#pragma link C++ class ROOT::RTF::RHdf5DS+;
#endif

#endif